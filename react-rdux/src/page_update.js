import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import {withRouter} from 'react-router-dom'

class UpdatedDevice extends Component {
	 
 constructor(props){
    super(props);
     
  }

  render(){
  	console.log("propsss",this.props)
    return(
      <div >
        <h1>Updated Devices is: {this.props.age}</h1>
      </div>
    )
  }
}
const mapDispachToProps = dispatch => {
  return {
    onAgeUp: () => dispatch({ type: "AGE_UP", value: 1 }),
    onAgeDown: () => dispatch({ type: "AGE_DOWN", value: 1 })
  };
};

const mapStateToProps = state => {
	console.log("state..",state)
  return {
    age: state.age
  };
};
//export default UpdatedDevice;
withRouter(connect(mapStateToProps)(UpdatedDevice))

export default UpdatedDevice;
//withRouter(connect(mapStateToProps)(UpdatedDevice))
