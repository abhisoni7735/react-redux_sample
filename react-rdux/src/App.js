import React, { Component } from "react";
import "./App.css";
import { connect } from "react-redux";
import { BrowserRouter as Router , Link, Route} from 'react-router-dom';
import UpdatedDevice from './page_update'
class App extends Component {
   routeAnotherComponent() {
    window.location.href="./page_update";
  }
  render() {
    return (
      <Router>
      <Route path="/" exact render={() => { return (
      <div className="App">
        <div className="Age-label">
          your age: <span>{this.props.age}</span>
        </div>
        <button onClick={this.props.onAgeUp}>Age UP</button>
        <button onClick={this.props.onAgeDown}>Age Down</button>
        <button onClick={this.routeAnotherComponent} className="submit-button">NEXT PAGE</button>
      </div>
      );}}/>
      <Route exact path='/page_update' render={(props) => <UpdatedDevice />}/>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    age: state.age
  };
};

const mapDispachToProps = dispatch => {
  return {
    onAgeUp: () => dispatch({ type: "AGE_UP", value: 1 }),
    onAgeDown: () => dispatch({ type: "AGE_DOWN", value: 1 })
  };
};
export default connect(
  mapStateToProps,
  mapDispachToProps
)(App);