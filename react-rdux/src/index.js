import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reducer from './store/reducer';

import { Provider } from 'react-redux';  //will allow us to inject global store
import { createStore } from 'redux';//we need to import store and build store

const store = createStore(reducer);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root')); //store is property
/*we gona wrap entire app wd provider. whatevr you pass to it wil be available globally*/